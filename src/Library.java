

public class Library {
	String name;
	String location;
	
	
	public String getName(){
		return name;
	}
	
	public String getLocation(){
		return location;
	}
	
	public static void main(String[] args){
	
		Student s1 = new Student("5610451035","thaprakorn","M.S.student");
		Student s2 = new Student("5610450012","jirawat","B.S.student");
		Student s3 = new Student("5610453452","jirakorn","D.R.student");
		Book book = new Book();
		book.ternBook("book1");
		book.ternBook("book2");
		book.ternBook("book3");
		book.ternBook("book4");
		
		System.out.println("book in storage: "+book.getInfo()+"\n"+"num book in storage: "+book.getcheckNum()+"\n");
		
		s1.borrow("book1");
		book.borrow("book1");
		s1.borrow("book2");
		book.borrow("book2");
		System.out.println("id: "+s1.getId()+"\n"+"name: "+s1.getName()+"\n"+"degree: "+s1.getDegree()+"\n"+"borrow: "
							+s1.getInfo()+"\n"+"book in storage: "+book.getInfo()+"\n"+"num book in storage: "+book.getcheckNum()+"\n");
		
		s2.ternBook("book1");
		book.borrow("book1");
		s2.borrow("book3");
		book.borrow("book3");
		System.out.println("id: "+s2.getId()+"\n"+"name: "+s2.getName()+"\n"+"degree: "+s2.getDegree()+"\n"+"borrow: "
				+s1.getInfo()+"\n"+"book in storage: "+book.getInfo()+"\n"+"num book in storage: "+book.getcheckNum()+"\n");
		
		s3.ternBook("book1");
		book.ternBook("book1");
		s3.ternBook("book2");
		book.ternBook("book2");
		s3.borrow("book3");
		book.borrow("book3");
		System.out.println("id: "+s3.getId()+"\n"+"name: "+s3.getName()+"\n"+"degree: "+s3.getDegree()+"\n"+"borrow: "
				+s3.getInfo()+"\n"+"book in storage: "+book.getInfo()+"\n"+"num book in storage: "+book.getcheckNum()+"\n");
		

	}

}
